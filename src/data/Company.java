package data;

import java.util.HashMap;

public class Company {
	
	private int orgNummer;
	private String navn, brukerNavn, passord;
	private String lokasjon;
	private HashMap<Integer, Stilling> utlysteStillinger;
	
	public Company(int id, String navn) {
		this.brukerNavn = "admin";
		this.orgNummer = id;
		this.passord = "1234";
		this.navn = navn;
		utlysteStillinger = new HashMap<>();
		
	}
	
	protected int companyScore(Employee emp) {
		
		int i = 0;
		
		for(Stilling stilling : utlysteStillinger.values()) {
			
			i+=Employeesorter.calculateScore(emp, stilling);
			
		}
		return i;
		
	}

	/**
	 * @return the utlysteStillinger
	 */
	public HashMap<Integer, Stilling> getUtlysteStillinger() {
		return utlysteStillinger;
	}

	/**
	 * @param utlysteStillinger the utlysteStillinger to set
	 */
	public void setUtlysteStillinger(HashMap<Integer, Stilling> utlysteStillinger) {
		this.utlysteStillinger = utlysteStillinger;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return orgNummer;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.orgNummer = id;
	}

	/**
	 * @return the navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * @param navn the navn to set
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * @return the lokasjon
	 */
	public String getLokasjon() {
		return lokasjon;
	}

	/**
	 * @param lokasjon the lokasjon to set
	 */
	public void setLokasjon(String lokasjon) {
		this.lokasjon = lokasjon;
	}

	/**
	 * @return the brukerNavn
	 */
	public String getBrukerNavn() {
		return brukerNavn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Company [orgNummer=" + orgNummer + ", navn=" + navn+ "]";
	}

	/**
	 * @return the orgNummer
	 */
	public int getOrgNummer() {
		return orgNummer;
	}

	/**
	 * @param orgNummer the orgNummer to set
	 */
	public void setOrgNummer(int orgNummer) {
		this.orgNummer = orgNummer;
	}

	/**
	 * @param brukerNavn the brukerNavn to set
	 */
	public void setBrukerNavn(String brukerNavn) {
		this.brukerNavn = brukerNavn;
	}

	/**
	 * @return the passord
	 */
	public String getPassord() {
		return passord;
	}

	/**
	 * @param passord the passord to set
	 */
	public void setPassord(String passord) {
		this.passord = passord;
	}

}
