package data;

public class Study {
	
	private Grade grade;
	private String field;
	private String description;
	
	public enum Grade {
		
		�rsstudium(0), Bachelor(1), Master(2), Doktor(1);
		private final int xp;
		private Grade(int xp) {
			this.xp = xp;
		}
		public int getXp() {
			return xp;
		}
	}
	
	/**
	 * @param length
	 * @param field
	 */
	public Study(String grade, String field, String description) {
		super();
		this.grade = Grade.valueOf(grade);
		this.field = field;
		this.description = description;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Study) {
			
			Study study = (Study) obj;
			if(this.field.equals(study.getField()) && this.description.equals(study.getDescription()) && study.getGrade().getXp() <= this.grade.getXp()) {
				return true;
			}
			
		}
		return false;
	}
	
	
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return field + ". Underkategori: " + description + ". Niv�: " + grade;
	}

	/**
	 * @return the grade
	 */
	public Grade getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	
	

}
