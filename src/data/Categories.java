package data;

import java.util.ArrayList;

public class Categories {
	
	private String name;
	private ArrayList<String> subCategories;
	
	public Categories(String name) {
		this.name = name;
		this.subCategories = new ArrayList<>();
		
		
	}
	
	
	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the subCategories
	 */
	public ArrayList<String> getSubCategories() {
		return subCategories;
	}

	/**
	 * @param subCategories the subCategories to set
	 */
	public void setSubCategories(ArrayList<String> subCategories) {
		this.subCategories = subCategories;
	}
}
