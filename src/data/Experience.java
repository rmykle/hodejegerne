package data;

public class Experience {
	
	
	private String field;
	private String description;
	private int length;
	
	
	/**
	 * @param field
	 * @param description
	 * @param length
	 */
	public Experience(String field, String description, int length) {
		super();
		this.field = field;
		this.description = description;
		this.length = length;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Experience) {
			
			Experience exp = (Experience) obj;
			if(this.field.equals(exp.getField()) && this.description.equals(exp.getDescription()) && this.length <= exp.getLength()){
				return true;
			}
			
		}
		return false;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return field + ". Underkategori: " + description + ". Lengde: " + length + " �r.";
	}
	
	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
