
package data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.sun.javafx.webkit.KeyCodeMap.Entry;

import collections.Warehouse;

public class Employeesorter {

	
	public static ArrayList<Employee> initiate(Collection<Employee> employees, Stilling stilling) {
		
		ArrayList<Employee> scores = new ArrayList<>(); // Holder scores
		
		// Legger til ansatte med score
		for(Employee emp : employees) {
			
			emp.setScore(calculateScore(emp, stilling));
			scores.add(emp);
			
			
		}
		
		// Sorterer
		Comparator<Employee> sorter = new Comparator<Employee>() {
			
			@Override
			public int compare(Employee o1, Employee o2) {
				
				if(o1.getScore() > o2.getScore()) return 1;
				else if(o2.getScore() > o1.getScore()) return -1;
				return 0;
				
			}
		};
		
		Collections.sort(scores, sorter);
		
		setScoreCap(scores);
		
		return scores;
		
	}

	private static void setScoreCap(ArrayList<Employee> scores) {
		int pivot = 0;
		
		
		if(scores.size() > 0) {
			pivot = scores.get(scores.size()-1).getScore();
			
			
		}
		
		for(Employee emp : scores) {
			if(pivot != 0) {
			emp.setScore((emp.getScore() * 100) / pivot);
			}
			
			
		}
		
		
	}

	public static int calculateScore(Employee emp, Stilling stilling) {
		
		int sum = 0;
		int totalReqs = stilling.getStudyReq().size() + stilling.getErfaring().size();
		
		
		//Study
		sum += studyScore(emp, stilling, totalReqs);
		
		//Exp
		sum += expScore(emp, stilling, totalReqs);
		
		
		
		
		return sum;
	}
	
	private static int studyScore(Employee emp, Stilling stilling, int totalReqs) {
		int sum = 0;
		
		for(Study study : emp.getStudies()) {
			
			if(stilling.getStudyReq().contains(study)) {
				sum += (100 / totalReqs);
			}
			
			else if(stilling.containsStudyField(study)) {
				sum += (100 / totalReqs) ;
			}
			
			sum += study.getGrade().getXp() * 2;
			
			
		}
		
		return sum;
	}
	
	private static int expScore(Employee emp, Stilling stilling, int totalReqs) {
		int sum = 0;
		for(Experience exp : emp.getWorkExperience()) {
			if(stilling.getErfaring().contains(exp)) sum += (100 / totalReqs);
			
			else if(stilling.containsExpField(exp)) {
				sum += (100/ totalReqs );
			}
			
			sum += exp.getLength() * 2;
			
		}
		
		return sum;
		
	}
	
	public static HashMap<Integer, Integer> getcompetencyMap(Collection<Employee> employees, Company company) {
		
		HashMap<Integer, Integer> finalMap = new HashMap<>();
		
		ArrayList<Integer> tempMap = new ArrayList<>();
		
		for(Employee emp : employees) {
			
			int sum = 0;
			
			for(Stilling stilling : company.getUtlysteStillinger().values()) {
				
				if(calculateScore(emp, stilling) >= 80) sum++;
				
			}
			
			tempMap.add(sum);
			
		}
		
		for(int sums : tempMap) {
			
			if(sums > 4) {
				sums = 4;
			}
			
			if(finalMap.get(sums) == null) {
				finalMap.put(sums, 1);
			}
			else {
				int holder = finalMap.get(sums) +1;
				finalMap.put(sums, holder);
			}
			
		}
		
		return finalMap;
		
		
	}
	
	public static Employee getGreatestCandidate(Warehouse house) {
		
		int score = 0;
		Employee high = null;
		for(Employee emp : house.getEmployees().values()) {
			int holdScore = 0;
			
			for(Study study : emp.getStudies()) {
				
				holdScore += study.getGrade().getXp()*2;
				
			}
			for(Experience exp : emp.getWorkExperience()) {
				
				int expScore = exp.getLength() / 2;
				holdScore +=  expScore;
				
			}
			if(score <= holdScore) {
				score = holdScore;
				high = emp;
			}
			
		}
		return high;
		
		
//		Employee highest = null;
//		int score = 0;
//		
//		for(Employee employee : house.getEmployees().values()) {
//			
//			int tempScore = 0;
//			
//			for(Company comp : house.getCompanies().values()) {
//				
//				tempScore += comp.companyScore(employee);
//				
//			}
//			
//			if(tempScore > score) {
//				highest = employee;
//			}
//			
//			
//			
//			
//		}
//		return highest;
		
	}
	
	
}
