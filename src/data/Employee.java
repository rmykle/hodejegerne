package data;

import java.util.ArrayList;

import data.Study.Grade;

public class Employee {
	
	private int id;
	private String navn, lokasjon;
	private int score;
	private long dob;
	private ArrayList<Study> studies;
	private ArrayList<Experience> workExperience;
	
	public Employee(int id, String navn,  int age) {

		this.id = id;
		this.navn = navn;
		this.studies = new ArrayList<>();
		this.workExperience = new ArrayList<>();
		this.score = 0;
		
	}
	
	public String competancyString() {
		String competancy = "Kandidat " + id + "<br>";
		int aarsStudium = 0;
		int bachelor = 0;
		int master = 0;
		int doktorgrad = 0;
		int erfaring = 0;
		
		for(Study study : studies) {
			switch (study.getGrade()) {
			case �rsstudium:
				aarsStudium++;
				break;
			case Bachelor:
				bachelor++;
				break;
			case Master:
				master++;
				break;
			case Doktor:
				doktorgrad++;
				break;
			default:
				break;
			}
		}
		for(Experience exp : workExperience) {
			erfaring += exp.getLength();
		}
		
		competancy +=
				"�rsstudium: " + aarsStudium + "<br>" +
				"Bachelor: " + bachelor + "<br>"+
				"Master: " + master + "<br>"+
				"Doktor " + doktorgrad + "<br>"+
				"Totalt arbeidserfaring " + erfaring + " �r"
				;
		return competancy;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * @param navn the navn to set
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	

	/**
	 * @return the studies
	 */
	public ArrayList<Study> getStudies() {
		return studies;
	}

	/**
	 * @param studies the studies to set
	 */
	public void setStudies(ArrayList<Study> studies) {
		this.studies = studies;
	}

	/**
	 * @return the workExperience
	 */
	public ArrayList<Experience> getWorkExperience() {
		return workExperience;
	}

	/**
	 * @param workExperience the workExperience to set
	 */
	public void setWorkExperience(ArrayList<Experience> workExperience) {
		this.workExperience = workExperience;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}


	

	/**
	 * @param dob the dob to set
	 */
	public void setDob(int dob) {
		this.dob = dob;
	}

	/**
	 * @return the lokasjon
	 */
	public String getLokasjon() {
		return lokasjon;
	}

	/**
	 * @param lokasjon the lokasjon to set
	 */
	public void setLokasjon(String lokasjon) {
		this.lokasjon = lokasjon;
	}

	/**
	 * @return the dob
	 */
	public long getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(long dob) {
		this.dob = dob;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	
	


}
