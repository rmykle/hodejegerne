package data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Stilling implements Comparable<Stilling> {
	
	private int id;
	private String stillingsTittel, stillingsBeskrivelse;
	private Date utlystDato;
	private ArrayList<Study> studyReq;
	private ArrayList<Experience> erfaring;
	
	public Stilling(int id, String stillingsTittel, String stillingsBeskrivelse, Date utlyst) {
		this.id = id;
		this.stillingsTittel = stillingsTittel;
		this.stillingsBeskrivelse = stillingsBeskrivelse;
		utlystDato = utlyst;
		this.studyReq = new ArrayList<>();
		this.erfaring = new ArrayList<>();
	}
	
	
	protected boolean containsStudyField(Study study) {
		
		for(Study stillingStudy : studyReq) {
			
			if(study.getField().equals(stillingStudy)) return true;
			
		}
		return false;
		
	}
	
	protected boolean containsExpField(Experience exp) {
	
		for(Experience stillingExp: erfaring) {
			
			if(exp.getField().equals(stillingExp.getField())) return true;
		
		}
		return false;
	
	}
	
	public String getFormattedDate() {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(utlystDato);
		
	}
	
	
	/**
	 * @return the stillingsTittel
	 */
	public String getStillingsTittel() {
		return stillingsTittel;
	}

	/**
	 * @param stillingsTittel the stillingsTittel to set
	 */
	public void setStillingsTittel(String stillingsTittel) {
		this.stillingsTittel = stillingsTittel;
	}

	/**
	 * @return the stillingsBeskrivelse
	 */
	public String getStillingsBeskrivelse() {
		return stillingsBeskrivelse;
	}

	/**
	 * @param stillingsBeskrivelse the stillingsBeskrivelse to set
	 */
	public void setStillingsBeskrivelse(String stillingsBeskrivelse) {
		this.stillingsBeskrivelse = stillingsBeskrivelse;
	}

	/**
	 * @return the utlystDato
	 */
	public Date getUtlystDato() {
		return utlystDato;
	}

	/**
	 * @param utlystDato the utlystDato to set
	 */
	public void setUtlystDato(Date utlystDato) {
		this.utlystDato = utlystDato;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}




	/**
	 * @return the studyReq
	 */
	public ArrayList<Study> getStudyReq() {
		return studyReq;
	}




	/**
	 * @param studyReq the studyReq to set
	 */
	public void setStudyReq(ArrayList<Study> studyReq) {
		this.studyReq = studyReq;
	}




	/**
	 * @return the erfaring
	 */
	public ArrayList<Experience> getErfaring() {
		return erfaring;
	}




	/**
	 * @param erfaring the erfaring to set
	 */
	public void setErfaring(ArrayList<Experience> erfaring) {
		this.erfaring = erfaring;
	}


	@Override
	public int compareTo(Stilling o) {
		if(this.utlystDato.before(o.getUtlystDato())) return -1;
		else if(this.utlystDato.after(o.getUtlystDato())) return 1;
		return 0;
		
	}

	

}
