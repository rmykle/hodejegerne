package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import collections.Warehouse;

public class CategoryDAO {
	
public static void getAllCategories(Warehouse house) {
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM kategori";
		try {
			stmt = conn.createStatement();
			
			ResultSet companies = stmt.executeQuery(getAllString);
			
			while(companies.next()) {
				
				String felt = companies.getString("Felt");
				String beskrivelse = companies.getString("Beskrivelse");
				
				house.hasFieldCategory(felt, beskrivelse);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}

}
