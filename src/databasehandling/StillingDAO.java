package databasehandling;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import collections.Warehouse;
import data.Stilling;
import tools.Logintools;

public class StillingDAO {

	
	public static void getStilling(Warehouse house, Logintools login) {
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM Stilling WHERE bedrift = " + login.getLoggedInCompany().getOrgNummer() ;
		try {
			stmt = conn.createStatement();
			
			ResultSet companies = stmt.executeQuery(getAllString);
			
			while(companies.next()) {
				
				int id = companies.getInt("stillingsId");
				String tittel = companies.getString("Tittel");
				String beskrivelse = companies.getString("Beskrivelse");
				Date utlyst = companies.getDate("Utlyst");
				int bedriftID = companies.getInt("bedrift");
				
				Stilling newStilling = new Stilling(id, tittel, beskrivelse, utlyst);
				
				
				house.getCompanies().get(bedriftID).getUtlysteStillinger().put(id, newStilling);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static int nyStilling(Stilling stilling, int bedriftID) {
		
		Connection conn = ConnectionFactory.getSqlFactory().createConnection();
		
		Statement stmt = null;
		
		String inputString = "INSERT INTO `hodejegere`.`stilling` (`Tittel`, `Beskrivelse`, `Utlyst`, `bedrift`) VALUES " 
				+ "('"+ stilling.getStillingsTittel() +"', '" + stilling.getStillingsBeskrivelse() +"', '"+ stilling.getFormattedDate() +"', '"+ String.valueOf(bedriftID) +"')";
			
		try {
			stmt = conn.createStatement();
			int affectedRows = stmt.executeUpdate(inputString);
			
			if(affectedRows == 0 ) {
			}
			
			else {
				
				
				ResultSet keys = stmt.getGeneratedKeys();
				if(keys.next()) {
					return keys.getInt(1);
				}
				
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	
}
