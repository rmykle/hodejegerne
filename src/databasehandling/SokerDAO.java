package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import collections.Warehouse;
import data.Employee;

public class SokerDAO {

	public static void getEmployees(Warehouse house) {
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM arbeidssoker";
			
		try {
			stmt = conn.createStatement();
			ResultSet companies = stmt.executeQuery(getAllString);
			
			while(companies.next()) {
				String navn = companies.getString("Navn");
				String lokasjon = companies.getString("City");
				long dob = companies.getLong("Personnummer");
				int id = companies.getInt("SokerID");
				
				Employee newEmp = new Employee(id, navn, 0);
				newEmp.setDob(dob);
				newEmp.setLokasjon(lokasjon);
				
				house.getEmployees().put(id, newEmp);
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
	}

	
	
}
