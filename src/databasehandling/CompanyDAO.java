package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import collections.Warehouse;
import data.Company;

public class CompanyDAO {

	
	public static void getCompanies(Warehouse house) {
		
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM Arbeidsgiver, giverbrukere where Organisasjonsnummer = orgNummer";
		try {
			stmt = conn.createStatement();
			
			ResultSet companies = stmt.executeQuery(getAllString);
			
			while(companies.next()) {
				int orgNummer = companies.getInt("Organisasjonsnummer");
				String name = companies.getString("Firmanavn");
				String lokasjon = companies.getString("By");
				String brukernavn = companies.getString("Brukernavn");
				String passord = companies.getString("passord");
				
				
				Company newCompany = new Company(orgNummer, name);
				newCompany.setLokasjon(lokasjon);
				newCompany.setBrukerNavn(brukernavn);
				newCompany.setPassord(passord);
				
				house.getCompanies().put(orgNummer, newCompany);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		
	}
	
	
	
}
