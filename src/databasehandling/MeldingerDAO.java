package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import collections.Warehouse;
import data.Company;
import messaging.Message;
import messaging.Messagebase;

public class MeldingerDAO {
	
	public static void getMessages(Messagebase msgBase, Warehouse house, int org) {
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM dialog WHERE Organisasjonsnummer = " +org;
			try {
				stmt = conn.createStatement();
				ResultSet companies = stmt.executeQuery(getAllString);
				
				while(companies.next()) {
					int bedrift = companies.getInt("Organisasjonsnummer");
					int sokerID = companies.getInt("SokerID");
					boolean companySends = companies.getInt("Sender") == 0;
					String melding = companies.getString("Melding");
					java.sql.Timestamp sendDate = companies.getTimestamp("Dato");
					
					Message newMessage = new Message(house.getCompanies().get(bedrift), house.getEmployees().get(sokerID), sendDate, companySends, melding);
					msgBase.newMessage(newMessage);
				}
				
				
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
		
		
		
	}

	public static void nyMelding(Message msg) {
		
			int sender = 1;
			if(msg.isCompanySends()) {
				sender = 0;
			}
			Connection conn = ConnectionFactory.getSqlFactory().createConnection();
			Statement stmt = null;
			
			String insertString = "INSERT INTO `hodejegere`.`dialog` (`Organisasjonsnummer`, `SokerID`, `Sender`, `Melding`, `Dato`) VALUES "
					+ "('"+ msg.getCompany().getOrgNummer()+"', "
					+ "'"+ msg.getEmployee().getId()+"', "
					+ "'"+ sender +"', "
					+ "'"+ msg.getContent()+"', "
					+ "CURRENT_TIMESTAMP)";
			
			try {
				stmt = conn.createStatement();
				stmt.executeUpdate(insertString);
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
				
				
				
		
		
	}

	public static ArrayList<Message> refreshMessages(Company loggedInCompany, Warehouse house, Messagebase msgBase) {
		int orgNummer = loggedInCompany.getOrgNummer();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.SECOND, -20);
		Date newDate = cal.getTime();
		String dateString = format.format(newDate);
		
		ArrayList<Message> tempList = new ArrayList<>();
		
		String getString = "SELECT * FROM dialog WHERE Organisasjonsnummer = " + orgNummer + " AND (Dato BETWEEN '"+ dateString +"' AND '2017-11-29 10:15:55')";
		
		Connection conn = ConnectionFactory.getSqlFactory().createConnection();
		
		Statement stmt;
		try {
			stmt = conn.createStatement();
			
			ResultSet messages = stmt.executeQuery(getString);
			
			while(messages.next()) {
				int bedrift = messages.getInt("Organisasjonsnummer");
				int sokerID = messages.getInt("SokerID");
				boolean companySends = messages.getInt("Sender") == 0;
				String melding = messages.getString("Melding");
				java.sql.Timestamp sendDate = messages.getTimestamp("Dato");
				
				Message newMessage = new Message(house.getCompanies().get(bedrift), house.getEmployees().get(sokerID), sendDate, companySends, melding);
				msgBase.newMessage(newMessage);
				tempList.add(newMessage);
				
			}
			
			
			conn.close();
			
			
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		
		return tempList;
	}

}
