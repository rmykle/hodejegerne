package databasehandling;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class ConnectionFactory {
	
private static ConnectionFactory sqlFactory = null;
	
	private static String driver = "com.mysql.jdbc.Driver";
	private static Connection connection = null;
	private static final String host = "bigfoot.uib.no";
	private static final String dbName = "gr5_16";
	private static final int port = 3306;
	private static String mySqlUrl = "jdbc:mysql://" + host + ":" + port;
	
	
	/**
	 * Method creates a connection to the database 
	 * @return the new connection
	 */
	public Connection createConnection() {

		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}  
		try {
			connection= (Connection) DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/hodejegere","root","");
			return connection;
		} catch (SQLException e) {
			e.printStackTrace();
		}  
		
		
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			System.err.println("RIP driverfeil");
//		}
//
//		Properties userInfo = new Properties();
//		userInfo.put("user", "i233_16_gr5");
//		userInfo.put("password", "gradte");
//
//		try {
//			connection = (Connection) DriverManager.getConnection(mySqlUrl, userInfo);
//			return connection;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
		return null;

	}

	private ConnectionFactory() {

	}
	

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the mySqlUrl
	 */
	public String getMySqlUrl() {
		return mySqlUrl;
	}

	



	/**
	 * @return the driver
	 */
	public static String getDriver() {
		return driver;
	}



	


	/**
	 * @return the dbname
	 */
	public static String getDbname() {
		return dbName;
	}



	


	/**
	 * @return the sqlFactory
	 */
	public static ConnectionFactory getSqlFactory() {
		if(sqlFactory == null) {
			sqlFactory = new ConnectionFactory();
		}
		return sqlFactory;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}






}
