package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

import collections.Warehouse;
import data.Experience;
import data.Stilling;
import tools.Logintools;

public class ErfaringDAO {

	
	public static void getAllExp(Warehouse house, Logintools login) {
		
		Connection conn = ConnectionFactory.getSqlFactory().getConnection();
		
		Statement stmt = null;
		
		String getAllString = "SELECT * FROM erfaring";
			
		try {
			stmt = conn.createStatement();
			ResultSet companies = stmt.executeQuery(getAllString);
			
			while(companies.next()) {
				
				int id = companies.getInt("SokerId");
				String felt = companies.getString("Felt");
				String beskrivelse = companies.getString("Kategori");
				int lengde = companies.getInt("Lengde");
				
				Experience exp = new Experience(felt, beskrivelse, lengde);
				house.getEmployees().get(id).getWorkExperience().add(exp);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String getAllStillingErfaringString = "SELECT * FROM stillingerfaring, stilling WHERE bedrift = " + login.getLoggedInCompany().getOrgNummer() + " AND Stillingerfaring.StillingsID = Stilling.stillingsId";
		try {
			ResultSet stilling = stmt.executeQuery(getAllStillingErfaringString);
			
			while(stilling.next()) {
				
				
				int stillingsID = stilling.getInt("StillingsID");
				String felt1 = stilling.getString("Felt");
				String beskrivelse1 = stilling.getString("Kategori");
				int lengde1 = stilling.getInt("Lengde");
				
				Experience exp = new Experience(felt1, beskrivelse1, lengde1);
				
				house.getCompanies().get(login.getLoggedInCompany().getOrgNummer()).getUtlysteStillinger().get(stillingsID).getErfaring().add(exp);
				
				
				
			}
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		
	}
		
	public static void nyStillingErfaring(Stilling stilling) {
		
		Connection conn = ConnectionFactory.getSqlFactory().createConnection();
		String insertString = "INSERT INTO `hodejegere`.`stillingerfaring` (`Felt`, `Kategori`, `Lengde`, `StillingsID`) VALUES (?, ?, ?," + stilling.getId() +")";
		
		try {
			PreparedStatement insert = (PreparedStatement) conn.prepareStatement(insertString);
			
			for(Experience exp : stilling.getErfaring()) {
				
				insert.setString(1, exp.getField());
				insert.setString(2, exp.getDescription());
				insert.setInt(3, exp.getLength());
				insert.executeUpdate();
				
			}
			conn.close();
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		
		
	}
	
	
}
