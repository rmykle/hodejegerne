package databasehandling;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

import collections.Warehouse;
import data.Stilling;
import data.Study;
import tools.Logintools;

public class StudyDAO {
	
	public static void getAllStudies(Warehouse house, Logintools login) {
		
			Connection conn = ConnectionFactory.getSqlFactory().getConnection();
			
			Statement stmt = null;
			
			String getAllString = "SELECT * FROM utdanning";
				
			try {
				stmt = conn.createStatement();
				ResultSet companies = stmt.executeQuery(getAllString);
				
				while(companies.next()) {
					
					int id = companies.getInt("SokerID");
					String felt = companies.getString("Felt");
					String beskrivelse = companies.getString("Kategori");
					String grad = companies.getString("Grad");
					Study newStudy = new Study(grad, felt, beskrivelse);
					
					house.getEmployees().get(id).getStudies().add(newStudy);
					
					
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			String getAllStillingUtdanningString = "SELECT * FROM stillingutdanning, stilling WHERE bedrift = " + login.getLoggedInCompany().getOrgNummer() + " AND StillingID = stillingsId";
			try {
				ResultSet stilling = stmt.executeQuery(getAllStillingUtdanningString);
				
				while(stilling.next()) {
					
					
					int stillingsID = stilling.getInt("StillingID");
					String felt1 = stilling.getString("Felt");
					String beskrivelse1 = stilling.getString("Kategori");
					String grad1 = stilling.getString("Grad");
					
					Study nyKrav = new Study(grad1, felt1, beskrivelse1);
					
					house.getCompanies().get(login.getLoggedInCompany().getOrgNummer()).getUtlysteStillinger().get(stillingsID).getStudyReq().add(nyKrav);
					
					
					
				}
				
				
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	
	public static void nyStillingStudie(Stilling stilling) {
		
		Connection conn = ConnectionFactory.getSqlFactory().createConnection();
		
		String insertString = "INSERT INTO `hodejegere`.`stillingutdanning` (`Felt`, `Kategori`, `Grad`, `StillingID`) VALUES (?, ?, ?," + stilling.getId() +")";
		
		try {
			PreparedStatement insert = (PreparedStatement) conn.prepareStatement(insertString);
			
			for(Study study : stilling.getStudyReq()) {
				
				insert.setString(1, study.getField());
				insert.setString(2, study.getDescription());
				insert.setString(3, study.getGrade().toString());
				insert.executeUpdate();
				
			}
			conn.close();
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		
		
	}

	}

