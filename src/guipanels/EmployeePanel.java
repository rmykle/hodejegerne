package guipanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import GUI.MainListener;
import data.Employee;
import data.Experience;
import data.Study;
import tools.GUItools;
import tools.Main;

public class EmployeePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private Employee employee;
	private JPanel infoPanel;
	private MainListener listen;
	private JButton kontaktSoker;
	
	
	public EmployeePanel(Employee employee, MainListener listen) {
		setLayout(new BorderLayout());
		this.listen = listen;
		this.employee = employee;
		setupPanel();
		buttonPanel();
		setbackGround();
	}

	private void setbackGround() {
		int score = 255;
		int empscore = (int) (employee.getScore());
		score -= empscore;
		
		
		
		
		Color bg = new Color(score, 255, score);
	
			
			setBackground(bg);
			
		
		
	}

	private void buttonPanel() {
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.white);
		kontaktSoker = new JButton("Kontakt s�ker");
		kontaktSoker.setBackground(GUItools.getCoolcolor());
		kontaktSoker.setForeground(Color.white);
		kontaktSoker.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				kontaktSoker.setBackground(GUItools.getCoolcolor());
				kontaktSoker.setForeground(Color.white);
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				kontaktSoker.setBackground(Color.white);
				kontaktSoker.setForeground(GUItools.getCoolcolor());
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		kontaktSoker.setActionCommand("contact" + employee.getId());
		kontaktSoker.addActionListener(listen);
		buttonPanel.add(kontaktSoker);
		
		URL url = Main.class.getResource("/yellowstar.png");
		ImageIcon icon = new ImageIcon(url);
		JLabel star = new JLabel(icon);
		if(employee.getScore() > 80) {
		buttonPanel.add(star);
		}
		
		add(buttonPanel, BorderLayout.SOUTH);
		
		
	}

	private void setupPanel() {
		
		infoPanel = new JPanel();
		infoPanel.setOpaque(false);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));
		infoPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, GUItools.getCoolcolor()));
		
		add(infoPanel, BorderLayout.CENTER);
		
		addUtdanning();
		addErfaring();
		addScore();
		
	}

	private void addScore() {
		JLabel poeng = GUItools.headerGenerator("Poeng:");
		poeng.setForeground(Color.black);
		infoPanel.add(poeng);
		infoPanel.add(GUItools.infoGenerator("Totalt: " + employee.getScore()));
		
	}

	private void addUtdanning() {
		JLabel header = GUItools.headerGenerator("Utdanning");
		header.setForeground(Color.black);
		infoPanel.add(header);
		
		
		String utdanningString = "";
		for(Study study : employee.getStudies()) {
			utdanningString += study + "<br>";
		}
		infoPanel.add(GUItools.infoGenerator(utdanningString));
		
	}

	private void addErfaring() {

		JLabel erfaringLabel = GUItools.headerGenerator("Erfaring");
		erfaringLabel.setForeground(Color.black);
		infoPanel.add(erfaringLabel);
		
		
		String erfaringsString = "";
		for(Experience erfaring : employee.getWorkExperience()) {
			
		erfaringsString += "- " +erfaring + "<br>";
		
		}
		
		infoPanel.add(GUItools.infoGenerator(erfaringsString));
		
	}

	/**
	 * @return the kontaktSoker
	 */
	public JButton getKontaktSoker() {
		return kontaktSoker;
	}

	

	
	
	
	
}
