package guipanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import GUI.MainListener;
import data.Experience;
import data.Stilling;
import data.Study;
import tools.GUItools;

public class PositionPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Stilling stilling;
	private JPanel infoPanel;
	private JButton showApplicants;
	private MainListener listen;

	
	public PositionPanel(Stilling stilling, MainListener listen) {
		this.listen = listen;
		this.stilling = stilling;
		setLayout(new BorderLayout());
		int height = 275 + (15 * stilling.getStudyReq().size()) + (15 * stilling.getErfaring().size());
		
		setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, GUItools.getCoolcolor()));
		setPreferredSize(new Dimension(500, height));
		setMaximumSize(getPreferredSize());
		
		buttonPanel();
		infoPanel();
		
	}


	private void infoPanel() {
		
		infoPanel = new JPanel();
		infoPanel.setBackground(Color.white);
		infoPanel.setMaximumSize(new Dimension(400, 1000));
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));
		
		infoPanel.add(GUItools.headerGenerator("ID"));
		infoPanel.add(GUItools.infoGenerator(String.valueOf(stilling.getId())));
		
		infoPanel.add(GUItools.headerGenerator("Tittel"));
		infoPanel.add(GUItools.infoGenerator(stilling.getStillingsTittel()));
		
		infoPanel.add(GUItools.headerGenerator("Stillingsbeskrivelse"));
		infoPanel.add(GUItools.infoGenerator(stilling.getStillingsBeskrivelse()));

		
		infoPanel.add(GUItools.headerGenerator("F�rst utlyst"));
		infoPanel.add(GUItools.infoGenerator(stilling.getUtlystDato().toString()));
		
		infoPanel.add(GUItools.headerGenerator("Krav til studier"));
		String studyKrav ="";
		if(stilling.getStudyReq().size() > 0 ) {
			for(Study studie : stilling.getStudyReq()) {
				studyKrav += studie + "<br>";
			}
		}
		else {
			studyKrav = "Ingen";
		}
		infoPanel.add(GUItools.infoGenerator(studyKrav));
		
		infoPanel.add(GUItools.headerGenerator("Krav til erfaring"));
		String erfaringKrav = "";
		if(stilling.getErfaring().size() > 0 ) {
			for(Experience exp : stilling.getErfaring()) {
				erfaringKrav += exp + "<br>";
			}
		}
		else {
			erfaringKrav = "Ingen";
		}
		infoPanel.add(GUItools.infoGenerator(erfaringKrav));
		

		
		
		
		add(infoPanel, BorderLayout.CENTER);
		
	}



	private void buttonPanel() {
		
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.setOpaque(false);
		
		JPanel innerButtonPanel = new JPanel(new GridLayout(3, 1));
		
		JButton endreButton = new JButton("Endre");
		endreButton.setBackground(GUItools.getCoolcolor());
		innerButtonPanel.add(endreButton);
		
		JButton sletteButton = new JButton("Slett");
		sletteButton.addActionListener(listen);
		sletteButton.setActionCommand("slett"+stilling.getId());
		sletteButton.setBackground(GUItools.getCoolcolor());
		innerButtonPanel.add(sletteButton);
		
		showApplicants = new JButton("Vis s�kere");
		showApplicants.setBackground(GUItools.getCoolcolor());
		showApplicants.setActionCommand("vis"+stilling.getId());
		showApplicants.addActionListener(listen);
		innerButtonPanel.add(showApplicants);
		
		buttonPanel.add(innerButtonPanel);
		
		buttonPanel.setPreferredSize(new Dimension(100, 250));
		
		add(buttonPanel, BorderLayout.EAST);
		
	}


	/**
	 * @return the showApplicants
	 */
	public JButton getShowApplicants() {
		return showApplicants;
	}


	/**
	 * @param showApplicants the showApplicants to set
	 */
	public void setShowApplicants(JButton showApplicants) {
		this.showApplicants = showApplicants;
	}
	
	
}
