package tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.HashMap;

import javax.swing.JLabel;

import collections.Warehouse;

public abstract class GUItools {
	
	private static final Color coolColor = new Color(37, 183, 196);
	private static final Color highColor = new Color(161, 232, 178);
	private static final Color lightGrey = new Color(229, 229, 229);
	
	public static JLabel headerGenerator(String text) {
	
		JLabel header = new JLabel(text);
		header.setOpaque(false);
		header.setFont(boldFont());
		header.setForeground(coolColor);
		return header;
	
	}
	
	public static JLabel infoGenerator(String text) {
		
		JLabel info = new JLabel("<html>" + text + "</html>");
		info.setOpaque(false);
		info.setFont(defaultFont());
		return info;
		
	}
	
	public static Font defaultFont() {
		
		Font newFont = new Font("Verdana", Font.PLAIN, 12);
		return newFont;
	}
	
	public static Font boldFont() {
		
		Font newFont = new Font("Verdana", Font.BOLD, 12);
		return newFont;
	}
	
	public static GridBagConstraints getDefaultConstraints() {
		GridBagConstraints cons = new GridBagConstraints();
		cons.gridwidth = GridBagConstraints.REMAINDER;
        cons.weightx = 1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.anchor = GridBagConstraints.NORTH;
        cons.insets = new Insets(0, 0, 5, 0);
        return cons;
		
	}

	public static Color getCoolcolor() {
		return coolColor;
	}

	public static Color getHighcolor() {
		return highColor;
	}

	public static Color getLightgrey() {
		return lightGrey;
	}
	
	
	

}
