package tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.sun.javafx.application.LauncherImpl;

import GUI.LoginFrame;
import GUI.MainFrame;
import GUI.MainListener;
import collections.Warehouse;
import data.Categories;
import data.Company;
import data.Employee;
import data.Employeesorter;
import data.Experience;
import data.Stilling;
import data.Study;
import databasehandling.CategoryDAO;
import databasehandling.CompanyDAO;
import databasehandling.ConnectionFactory;
import databasehandling.ErfaringDAO;
import databasehandling.MeldingerDAO;
import databasehandling.SokerDAO;
import databasehandling.StillingDAO;
import databasehandling.StudyDAO;
import messaging.Message;
import messaging.Messagebase;

public class Main {
	
	private static LoginFrame loginFrame;
	private static Warehouse house;
	private static Logintools login;
	private static Messagebase msgBase;
	
	public static void main(String[] args) {
		boolean debugging = true;
		
		house = new Warehouse();
		login = new Logintools(house);
		
		
		// DATABASE
				boolean dataBaseOK = false;
				ConnectionFactory.getSqlFactory().createConnection();
				if(ConnectionFactory.getSqlFactory().getConnection() != null){
					dataBaseOK = true;
					CompanyDAO.getCompanies(house);
					SokerDAO.getEmployees(house);
					
					try {
						ConnectionFactory.getSqlFactory().getConnection().close();
					
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				else {
					initiatePlanB(house, login);
				}
		
		
		if(!debugging && dataBaseOK) {
			new LoginFrame(login, dataBaseOK);
		}
		else {
			if(login.getLoggedInCompany() == null) {
			login.setLoggedInCompany(house.getCompanies().get(123321123));
			}
			initiate();
		}
	
	}
	
	private static void initiatePlanB(Warehouse house, Logintools login) {
		Company exampleCompany = new Company(0, "Rogers Røntgenstudio");
		
		login.setLoggedInCompany(exampleCompany);
		house.getEmployees().put(1, new Employee(1, "Bob", 12));
		login.getLoggedInCompany().getUtlysteStillinger().put(1, new Stilling(1, "Sykepleier", "Generell pleie", new Date()));
		
	}

	public static void initiate() {
		msgBase = new Messagebase(login.getLoggedInCompany());
		ConnectionFactory.getSqlFactory().createConnection();
		if(ConnectionFactory.getSqlFactory().createConnection() != null) {
			StillingDAO.getStilling(house, login);
			StudyDAO.getAllStudies(house, login);
			ErfaringDAO.getAllExp(house, login);
			MeldingerDAO.getMessages(msgBase, house, login.getLoggedInCompany().getOrgNummer());
			CategoryDAO.getAllCategories(house);
			try {
				ConnectionFactory.getSqlFactory().getConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		MainListener listen = new MainListener(house, login, msgBase);
		MainFrame main = new MainFrame(listen);
		listen.setMainFrame(main);
		
		
	}


	
	
}
