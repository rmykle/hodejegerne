package tools;

import java.util.Date;

import collections.Warehouse;
import data.Company;

public class Logintools {
	
	private Company loggedInCompany;
	private Warehouse house;
	private String password;
	private Date lastLogin;
	
	public Logintools(Warehouse house) {
		this.house = house;
	}

	public Company getLoggedInCompany() {
		return loggedInCompany;
	}

	public void setLoggedInCompany(Company loggedInCompany) {
		this.loggedInCompany = loggedInCompany;
	}
	
	public boolean checkUserLogin(String username, char[] password) {
		
		Company company = house.getCompanyByUserName(username);
		if(company != null) {
			String pwString = new String(password);
			
			if(username.equals(company.getBrukerNavn()) && pwString.equals(company.getPassord())) {
				
				loggedInCompany = company;
				return true;
			}
		}
		return false;
		
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	
	
	

}
