package tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Datetools {
	
	public static boolean checkDateString(String date) {
	
		return date.matches("^[0-3]?[0-9]/[0-1]?[0-9]/(?:[0-9]{2})?[0-9]{2}$");
	
	}
	
	public static boolean checkDateStringMinutes(String date) {
		
		
		return true;
	}
	
	public static Date stringToDate(String dateString) {
		
		if(checkDateString(dateString)) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return format.parse(dateString);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		}
		
		return null;
		
	}
	
	public static String dateToString(Date date) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		return format.format(date);
		
		
	}
	public static Date currentDate() {
		
		return new Date();
	
	}
	
	public static Date stringToDateMinutes(String dateString) {
		
		if(checkDateString(dateString)) {
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm");
			try {
				return format.parse(dateString);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		}
		return null;
		
	}

}
