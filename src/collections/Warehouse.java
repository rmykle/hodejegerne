package collections;

import java.util.ArrayList;
import java.util.HashMap;

import data.Categories;
import data.Company;
import data.Employee;

public class Warehouse {
	private HashMap<Integer, Employee> employees;
	private HashMap<Integer, Company> companies;
	private ArrayList<Categories> categories;
	
	public Warehouse() {

		this.employees = new HashMap<Integer, Employee>();
		this.companies = new HashMap<>();
		this.categories = new ArrayList<>();
		
	}
	
	public Company getCompanyByUserName(String userName) {
		
		for(Company company : companies.values()) {
			
			if(company.getBrukerNavn().equals(userName)) return company;
			
		}
		return null;
		
	}
	
	public void hasFieldCategory(String felt, String beskrivelse) {
		
		for(Categories cat : categories) {
			
			if(cat.getName().equals(felt)) {
				
				cat.getSubCategories().add(beskrivelse);
				return;
			}
			
		}
		Categories newCat = new Categories(felt);
		newCat.getSubCategories().add(beskrivelse);
		categories.add(newCat);
		
	}
	
	
	/**
	 * @return the employees
	 */
	public HashMap<Integer, Employee> getEmployees() {
		return employees;
	}

	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(HashMap<Integer, Employee> employees) {
		this.employees = employees;
	}

	/**
	 * @return the companies
	 */
	public HashMap<Integer, Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(HashMap<Integer, Company> companies) {
		this.companies = companies;
	}

	/**
	 * @return the categories
	 */
	public ArrayList<Categories> getCategories() {
		return categories;
	}

	/**
	 * @param categories the categories to set
	 */
	public void setCategories(ArrayList<Categories> categories) {
		this.categories = categories;
	}
	
	
	

}
