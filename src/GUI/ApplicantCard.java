package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import data.Employee;
import data.Stilling;
import guipanels.EmployeePanel;
import tools.GUItools;

public class ApplicantCard extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private MainListener listen;
	private JPanel jobPanel;
	private JLabel selectedStilling;
	public ApplicantCard(MainListener listen) {
		
		
		this.listen = listen;
		setLayout(new BorderLayout());
		
		selectedStilling = new JLabel("Valgt stilling:");
		selectedStilling.setFont(getFont().deriveFont(Font.PLAIN));
		setupPanel();
	}
	
	protected void updateHeader(Stilling stilling) {
		String text = "Valgt stilling: " + stilling.getStillingsTittel() + " - ";
		if(stilling.getStillingsBeskrivelse().length() > 50) {
			text += stilling.getStillingsBeskrivelse().substring(0, 50) + ".....";
		}
		else text += stilling.getStillingsBeskrivelse();
		selectedStilling.setText(text);
		
	}
	
	private void setupPanel() {
			
			jobPanel = new JPanel(new GridBagLayout());
			
			JPanel outerholder = new JPanel(new BorderLayout());
			
			JPanel header = new JPanel();
			header.add(selectedStilling);
			
			outerholder.add(header, BorderLayout.NORTH);
			
			JPanel posHolder = new JPanel(new BorderLayout());
			posHolder.add(jobPanel, BorderLayout.NORTH);
			JScrollPane scroll = new JScrollPane(posHolder);
			outerholder.add(scroll, BorderLayout.CENTER);
			add(outerholder, BorderLayout.CENTER);
			validate();
			repaint();
			
			}
		
	protected void refreshContent(ArrayList<Employee> sortedEmps) {
		jobPanel.removeAll();
		
		
		GridBagConstraints cons = GUItools.getDefaultConstraints();
		for(Employee applicant : sortedEmps) {
			
			if(applicant.getScore() > 30) {
			EmployeePanel empPanel = new EmployeePanel(applicant, listen);
			jobPanel.add(empPanel, cons, 0);
			}
			
			
			
		}
		
		validate();
		repaint();
		
	}
}
