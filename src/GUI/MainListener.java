package GUI;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import GUI.MainFrame.menuButton;
import collections.Warehouse;
import data.Employee;
import data.Employeesorter;
import data.Stilling;
import databasehandling.ErfaringDAO;
import databasehandling.MeldingerDAO;
import databasehandling.StillingDAO;
import databasehandling.StudyDAO;
import messaging.Message;
import messaging.Messagebase;
import tools.Datetools;
import tools.GUItools;
import tools.Logintools;

public class MainListener implements ActionListener {

	private MainFrame mainFrame;
	private Warehouse house;
	private Logintools tools;
	private Messagebase msgBase;
	private Stilling currentStilling;
	private Timer timer;
	private boolean timerRunning;
	
	public MainListener(Warehouse house, Logintools tools, Messagebase msgBase) {
		this.house = house;
		this.tools = tools;
		this.msgBase = msgBase;
		this.timer = new Timer();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Meny sjekk
		if(isMenuButton(e)) changeCard(e);
		
		// FRA NY STILLING KORT
		else if(e.getSource().equals(mainFrame.getNewPosCard().getReqButton())) {
			new RequirementFrame(mainFrame.getNewPosCard(), this);
		}
		
		else if(e.getSource().equals(mainFrame.getNewPosCard().getSubmit())) {
			
			NewPositionCard posCard = mainFrame.getNewPosCard();
			if(posCard.validInput()) {
				if(mainFrame.getNewPosCard().validInput()) {
				JOptionPane.showMessageDialog(null, "Ny stilling registert", "", JOptionPane.INFORMATION_MESSAGE);
				createNewPosition(posCard);
				CardLayout layout = (CardLayout) mainFrame.getCardHolder().getLayout();
				layout.show(mainFrame.getCardHolder(), "jobCard");
				}
				
				
			}
			
		}
		
		else if(e.getActionCommand().startsWith("contact")) {
			int empID = Integer.valueOf(e.getActionCommand().replace("contact", ""));
			mainFrame.getInboxCard().setIdSelected(empID);
			contactSearcher(empID);
			
			
		}
		
		// Inbox
		else if(e.getSource().equals(mainFrame.getInboxCard().getSendMelding())) {
			if(mainFrame.getInboxCard().getIdSelected() != 0) {
			nyMelding();
			}
			
		}
		
		else if(e.getSource().equals(mainFrame.getInboxCard().getOppdaterMeldinger())) {
			
			mainFrame.getInboxCard().refreshContent();
			if(mainFrame.getInboxCard().getIdSelected() != 0) {
			mainFrame.getInboxCard().employeeSelected();
			}
			
		}
		
		// Position overview card
		else if(e.getActionCommand().startsWith("vis")) {
			int id = Integer.parseInt(e.getActionCommand().replace("vis", ""));
			this.currentStilling = tools.getLoggedInCompany().getUtlysteStillinger().get(id);
			mainFrame.getSearchCard().updateHeader(currentStilling);
			
			ArrayList<Employee> sortedEmployees = Employeesorter.initiate(house.getEmployees().values(), currentStilling);
			mainFrame.getSearchCard().refreshContent(sortedEmployees);
			
			CardLayout layout = (CardLayout) mainFrame.getCardHolder().getLayout();
			layout.show(mainFrame.getCardHolder(), "searchCard");
			
			
		}
		
		
		else if(e.getActionCommand().startsWith("slett")) {
			int id = Integer.parseInt(e.getActionCommand().replace("slett", ""));
			if(JOptionPane.showConfirmDialog(null, "�nsker du � slette stillingen " + tools.getLoggedInCompany().getUtlysteStillinger().get(id).getStillingsTittel()+"?",
					"Bekreft", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
				tools.getLoggedInCompany().getUtlysteStillinger().remove(id);
				mainFrame.getJobCard().refreshPanels();
				
			}
			
		}
		
		else if(e.getSource().equals(mainFrame.getJobCard().getNyStilling())) {
			CardLayout layout = (CardLayout) mainFrame.getCardHolder().getLayout();
			layout.show(mainFrame.getCardHolder(), "newPosCard");
			
		}
		
		
		
	}
	
	
	private void contactSearcher(int empID) {
		mainFrame.setPressedButton(mainFrame.getFiller4());
		mainFrame.resetMenuColors();
		
		mainFrame.getFiller4().setOpaque(true);
		mainFrame.getFiller4().setBackground(Color.white);
		mainFrame.getFiller4().setForeground(GUItools.getCoolcolor());
		
		CardLayout layout = (CardLayout) mainFrame.getCardHolder().getLayout();
		Employee employee = house.getEmployees().get(empID);
		
		if(msgBase.getMessages().get(employee) == null) {
			msgBase.getMessages().put(employee, new ArrayList<>());
		}
		mainFrame.getInboxCard().setIdSelected(empID);
		mainFrame.getInboxCard().refreshContent();
		mainFrame.getInboxCard().employeeSelected();
		layout.show(mainFrame.getCardHolder(), "inboxCard");
		
	}

	private void nyMelding() {
		Employee emp = house.getEmployees().get(mainFrame.getInboxCard().getIdSelected());
		Message msg = new Message(tools.getLoggedInCompany(), emp, Datetools.currentDate(), true, mainFrame.getInboxCard().getMeldingArea().getText());
		if(msgBase.getMessages().get(emp) == null) {
			msgBase.getMessages().put(emp, new ArrayList<>());
		}
		msgBase.getMessages().get(emp).add(msg);
		MeldingerDAO.nyMelding(msg);
		mainFrame.getInboxCard().getMeldingArea().setText("");
		mainFrame.getInboxCard().employeeSelected();
		mainFrame.getInboxCard().refreshContent();
		
	}

	private void createNewPosition(NewPositionCard posCard) {
		
		String tittel = posCard.getTittel().getText();
		String beskrivelse = posCard.getBeskrivelse().getText();
		
		int newId = 0;
		
		Stilling nyStilling = new Stilling(0, tittel, beskrivelse, new Date());
		
		nyStilling.setErfaring(new ArrayList<>(posCard.getErfaring().values()));
		nyStilling.setStudyReq(new ArrayList<>(posCard.getStudyReq().values()));
		posCard.getStudyReq().clear();
		posCard.getErfaring().clear();
		
		newId = StillingDAO.nyStilling(nyStilling, tools.getLoggedInCompany().getOrgNummer());
		nyStilling.setId(newId);
		
		StudyDAO.nyStillingStudie(nyStilling);
		ErfaringDAO.nyStillingErfaring(nyStilling);
		
		tools.getLoggedInCompany().getUtlysteStillinger().put(newId, nyStilling);
		
		mainFrame.getJobCard().refreshPanels();
		
		
	}

	protected void changeCard(ActionEvent e) {
		JButton button = (JButton) e.getSource();
			
		if(button.getName().equals("inboxCard") || button.getActionCommand().startsWith("cont")) {
			mainFrame.resetMenuColors();
			mainFrame.getFiller4().setOpaque(true);
			mainFrame.getFiller4().setForeground(GUItools.getCoolcolor());
			initiateUpdater();
		}
		else if(timerRunning && !button.getName().equals("inboxCard")) {
			timerRunning = false;
			timer.cancel();
			timer.purge();
			
		}
		
			CardLayout layout = (CardLayout) mainFrame.getCardHolder().getLayout();
			layout.show(mainFrame.getCardHolder(), button.getName());
		
		
		
		
	}
	
	protected void initiateUpdater() {
//		mainFrame.getInboxCard().refreshContent();
//		mainFrame.getInboxCard().employeeSelected();
		
		if(!timerRunning) {
			
			if(mainFrame.getInboxCard().getIdSelected() != 0) {
			
			timer = new Timer();
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					timerRunning = true;
					ArrayList<Message> foundMessage = MeldingerDAO.refreshMessages(tools.getLoggedInCompany(),house, msgBase );
					if(foundMessage.size() > 0) {
					mainFrame.getInboxCard().refreshContent();
					mainFrame.getInboxCard().employeeSelected();
					}
					
				}
			}, 0, 20000);
			}
		}
		
		
		
	}

	
	private boolean isMenuButton(ActionEvent e) {
		if(e.getSource() instanceof menuButton) {
			return true;
		}
		return false;
	}

	/**
	 * @return the mainFrame
	 */
	public MainFrame getMainFrame() {
		return mainFrame;
	}

	/**
	 * @return the house
	 */
	public Warehouse getHouse() {
		return house;
	}

	/**
	 * @return the tools
	 */
	public Logintools getTools() {
		return tools;
	}

	/**
	 * @return the msgBase
	 */
	public Messagebase getMsgBase() {
		return msgBase;
	}

	/**
	 * @return the currentStilling
	 */
	public Stilling getCurrentStilling() {
		return currentStilling;
	}

	/**
	 * @param mainFrame the mainFrame to set
	 */
	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	/**
	 * @param house the house to set
	 */
	public void setHouse(Warehouse house) {
		this.house = house;
	}

	/**
	 * @param tools the tools to set
	 */
	public void setTools(Logintools tools) {
		this.tools = tools;
	}

	/**
	 * @param msgBase the msgBase to set
	 */
	public void setMsgBase(Messagebase msgBase) {
		this.msgBase = msgBase;
	}

	/**
	 * @param currentStilling the currentStilling to set
	 */
	public void setCurrentStilling(Stilling currentStilling) {
		this.currentStilling = currentStilling;
	}
	
	
	
	
	

}
