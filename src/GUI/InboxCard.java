package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import data.Employee;
import messaging.Message;
import tools.Datetools;
import tools.GUItools;

public class InboxCard extends JPanel {

	private static final long serialVersionUID = 1L;
	private MainListener listen;
	private JPanel recieverPanel, messagePanel;
	private MouseListen mouseListen;
	private JTextArea meldingArea;
	private JButton sendMelding, oppdaterMeldinger;
	private int idSelected;
	private JScrollPane scroll;

	
	public InboxCard( MainListener listen) {
		this.listen = listen;
		this.mouseListen = new MouseListen();
		this.idSelected = 0;
		setLayout(new BorderLayout());
		
	
		setupRecieverPanel();
		refreshContent();
		
		setupMessagePanel();
		
		
	}

	

	private void setupMessagePanel() {
		
		JPanel rightSide = new JPanel(new BorderLayout());
		messagePanel = new JPanel(new GridBagLayout());
		JPanel filler = new JPanel(new BorderLayout());
		filler.add(messagePanel, BorderLayout.NORTH);
		scroll = new JScrollPane(filler);
		rightSide.add(scroll, BorderLayout.CENTER);
		
		rightSide.add(nyMelding(), BorderLayout.SOUTH);
		add(rightSide, BorderLayout.CENTER);
		
	}



	private Component nyMelding() {
		JPanel newMessagePanel = new JPanel(new BorderLayout());
		
		meldingArea = new JTextArea();
		meldingArea.setLineWrap(true);
		meldingArea.setWrapStyleWord(true);
		meldingArea.setPreferredSize(new Dimension(700, 150));
		
		JPanel headerPanel = new JPanel(new BorderLayout());
		headerPanel.add(GUItools.headerGenerator("Meldingtekst:"), BorderLayout.WEST);
		newMessagePanel.add(headerPanel, BorderLayout.NORTH);
		newMessagePanel.add(meldingArea, BorderLayout.CENTER);
		
		
		JPanel buttonPanel = new JPanel(new BorderLayout());
		
		JPanel innerButtonPanel = new JPanel(new FlowLayout());
		sendMelding = new JButton("Send");
		sendMelding.setBackground(GUItools.getCoolcolor());
		sendMelding.addActionListener(listen);
		
		oppdaterMeldinger = new JButton("Oppdater");
		oppdaterMeldinger.addActionListener(listen);
		oppdaterMeldinger.setBackground(GUItools.getCoolcolor());
		
		innerButtonPanel.add(sendMelding);
		//innerButtonPanel.add(oppdaterMeldinger);
		
		buttonPanel.add(innerButtonPanel, BorderLayout.WEST);
		
		
		newMessagePanel.add(buttonPanel, BorderLayout.SOUTH);
		
		
		
		
		return newMessagePanel;
	}

	private void setupRecieverPanel() {
		
		JPanel outerRecPanel = new JPanel(new BorderLayout());
		outerRecPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, GUItools.getCoolcolor()));
		outerRecPanel.setPreferredSize(new Dimension(250, 100));
		
		recieverPanel = new JPanel(new GridBagLayout());
		outerRecPanel.add(recieverPanel, BorderLayout.NORTH);
		add(outerRecPanel, BorderLayout.WEST);
		
		
	}
	
	protected void refreshContent() {
		
		GridBagConstraints cons = GUItools.getDefaultConstraints();
        
        
        recieverPanel.removeAll();
        recieverPanel.repaint();
        
        for(Employee employee : listen.getMsgBase().getSortedRecievers()) {
        	if(listen.getMsgBase().getMessages().get(employee) != null) {
        		if(employee != null) {
        	recieverPanel.add(selectionPanel(employee), cons, 0);
        		}
        	}
        	
        }
        
        
        recieverPanel.revalidate();
		
	}
	
	
	private JPanel selectionPanel(Employee employee) {
		
		JPanel employeeSelectionPanel = new JPanel(new BorderLayout());
		employeeSelectionPanel.setName(String.valueOf(employee.getId()));
		employeeSelectionPanel.addMouseListener(mouseListen);
		
		employeeSelectionPanel.setBackground(Color.white);
		
		JPanel upperInner = new JPanel(new BorderLayout());
		upperInner.setOpaque(false);
		JLabel navn = new JLabel("Kandidat#"+employee.getId());
		Message sisteMelding = listen.getMsgBase().getLatestMessage(employee);
		JLabel tid = new JLabel(Datetools.dateToString(sisteMelding.getDate()));
		upperInner.add(navn, BorderLayout.WEST);
		upperInner.add(tid, BorderLayout.EAST);
		
		
		String sisteMeldingString = sisteMelding.getContent();
		if(sisteMeldingString.length() > 30) {
			sisteMeldingString = sisteMeldingString.substring(0, 30) + "...";
		}
		JLabel sisteMeldingLabel = GUItools.infoGenerator(sisteMeldingString);
		
		employeeSelectionPanel.add(upperInner, BorderLayout.NORTH);
		employeeSelectionPanel.add(sisteMeldingLabel, BorderLayout.CENTER);
		employeeSelectionPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, GUItools.getCoolcolor()));
		return employeeSelectionPanel;

	}
	
	protected void employeeSelected() {
		
		Employee emp = listen.getHouse().getEmployees().get(idSelected);
		ArrayList<Message> sortedMessages = listen.getMsgBase().getSortedMessages(emp);
		
		GridBagConstraints cons = GUItools.getDefaultConstraints();
		messagePanel.removeAll();
		
		for(Message message : sortedMessages) {
			messagePanel.add(messagePanel(message), cons);
		}
		
		
		revalidate();
		repaint();
		
		
	}

	
	
	private JPanel messagePanel(Message msg) {
		JPanel innerMessagePanel = new JPanel(new BorderLayout());
//		innerMessagePanel.setPreferredSize(new Dimension(200, 75));
		innerMessagePanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1,0, GUItools.getCoolcolor()));
		
		JPanel upper = new JPanel(new BorderLayout());
		upper.setBackground(GUItools.getLightgrey());
		
		String sender ="";
		if(msg.isCompanySends()) sender = msg.getCompany().getNavn();
		else sender = "Kandidat#" +msg.getEmployee().getId();
		upper.add(GUItools.headerGenerator(sender), BorderLayout.WEST);
		upper.add(GUItools.infoGenerator(Datetools.dateToString(msg.getDate())), BorderLayout.EAST);
		
		innerMessagePanel.add(upper, BorderLayout.NORTH);
		innerMessagePanel.setBackground(Color.white);
		JPanel filler = new JPanel(new BorderLayout());
		filler.setBackground(Color.white);
		
		JTextArea area = new JTextArea(msg.getContent(), 1,1);
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		area.setFocusable(false);
		area.setEditable(false);
		area.setOpaque(false);
		
		
		
		filler.add(area, BorderLayout.NORTH);
		innerMessagePanel.add(filler, BorderLayout.CENTER);
		
		
		return innerMessagePanel;
	}
	
	

private class MouseListen implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		JPanel panel = (JPanel) e.getSource();
		idSelected = Integer.valueOf(panel.getName());
		employeeSelected();
		listen.initiateUpdater();
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}
	
	
}
	


	public JButton getSendMelding() {
		return sendMelding;
	}


	/**
	 * @return the meldingArea
	 */
	public JTextArea getMeldingArea() {
		return meldingArea;
	}

	/**
	 * @return the idSelected
	 */
	public int getIdSelected() {
		return idSelected;
	}

	/**
	 * @param idSelected the idSelected to set
	 */
	public void setIdSelected(int idSelected) {
		this.idSelected = idSelected;
	}



	/**
	 * @return the oppdaterMeldinger
	 */
	public JButton getOppdaterMeldinger() {
		return oppdaterMeldinger;
	}

	

	
	
	
}
