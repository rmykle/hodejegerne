package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import tools.GUItools;
import tools.Logintools;
import tools.Main;

public class LoginFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private static final Color defaultColor = new Color(255,191, 60);
	private JTextField userName;
	private JPasswordField passWord;
	private JButton submit;
	private JLabel loginAttempts;
	private Logintools tools;
	private JPanel loginPanel;
	private boolean databaseOK;
	public LoginFrame(Logintools tools, boolean dataBaseOK) {
		this.databaseOK = dataBaseOK;
		this.tools = tools;
		setLayout(new GridBagLayout());
		setLocationRelativeTo(null);
		getContentPane().setBackground(GUItools.getCoolcolor());
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setupWrapper();
		
		setBackground(GUItools.getCoolcolor());
		
		setSize(new Dimension(600, 600));
		
		setVisible(true);
		
		
		
		
	}
	private void setupWrapper() {
		
		loginPanel = new JPanel(new GridBagLayout());
		loginPanel.setBackground(Color.WHITE);
		loginPanel.setPreferredSize(new Dimension(350, 350));
		loginPanel.setMaximumSize(loginPanel.getPreferredSize());
		
		addFields();
		
		add(loginPanel);
		
		
	}
	private void addFields() {
		JPanel holderPanel = new JPanel();
		holderPanel.setOpaque(false);
		holderPanel.setPreferredSize(new Dimension(200, 130));
		holderPanel.setLayout(new BoxLayout(holderPanel, BoxLayout.PAGE_AXIS));
		userName = new JTextField();
		holderPanel.add(GUItools.headerGenerator("Brukernavn"));
		holderPanel.add(userName);
		
		passWord = new JPasswordField();
		holderPanel.add(GUItools.headerGenerator("Passord"));
		holderPanel.add(passWord);
		
		submit = new JButton("Logg inn");
		submit.addActionListener(new loginListener());
		
		holderPanel.add(submit);
		
		loginAttempts = new JLabel("");
		if(!databaseOK) {
			loginAttempts.setText("Kan ikke koble til database");
		}
		loginAttempts.setForeground(Color.red);
		holderPanel.add(loginAttempts);
		
		loginPanel.add(holderPanel);
		
		
		
	}
	
	private class loginListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			boolean valid = tools.checkUserLogin(userName.getText(), passWord.getPassword());
			if(valid) {
			Main.initiate();
			dispose();
			}
			else {
				
				loginAttempts.setText("Feil brukernavn eller passord. ");
				
			}
			
		}
		
		
	}
	
	
	public boolean isLoggedIn() {
		return true;
	}
	
	

}
