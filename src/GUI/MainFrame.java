package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import tools.GUItools;
import tools.Main;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private MainListener listen;
	private ApplicantCard searchCard;
	private FrontCard frontCard;
	private PositionOverviewCard jobCard;
	private NewPositionCard newPosCard;
	private JPanel cardHolder;
	private InboxCard inboxCard;
	private menuButton filler0, filler1, filler2, filler3, filler4;
	private static final Color color = GUItools.getCoolcolor();
	private JButton pressedButton;
	
	public MainFrame(MainListener listen) {
		
		
		
		
		//Customize frame
		setSize(950, 700);
		setTitle("Hodejeger");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		URL url = Main.class.getResource("/bullseye.png");
		ImageIcon icon = new ImageIcon(url);
		setIconImage(icon.getImage());
		leftVoid();
		this.listen = listen;
		
		//Set up main frame card holder
		cardHolder = new JPanel();
		cardHolder.setLayout(new CardLayout());
		
		// FrontCard
		frontCard = new FrontCard(listen);
		cardHolder.add(frontCard, "frontCard");;
	
		//Add inboxcard
		inboxCard = new InboxCard(listen);
		cardHolder.add(inboxCard, "inboxCard");
		
		//Add newPosCard
		newPosCard = new NewPositionCard(listen);
		cardHolder.add(newPosCard, "newPosCard");
		
		// Add searchcard
		searchCard = new ApplicantCard(listen);
		cardHolder.add(searchCard, "searchCard");
		
		// Add jobcard
		jobCard = new PositionOverviewCard(listen);
		cardHolder.add(jobCard, "jobCard");
		
		
		add(cardHolder, BorderLayout.CENTER);
		
		//Setup left menu
		setupLeftMenu();
		
		//Setup top menu
//		setupTopMenu();
		
		setVisible(true);
		
	}
	
	
	private void leftVoid() {
		
		JPanel leftVoid = new JPanel();
		leftVoid.setOpaque(false);
		leftVoid.setPreferredSize(new Dimension(15, 0));
		add(leftVoid, BorderLayout.WEST);
		
	}
	


	private void setupLeftMenu() {
		JPanel fillerPanel = new JPanel(new GridLayout(1,4));
		JPanel reallyFillerPanel = new JPanel();
		reallyFillerPanel.setOpaque(false);
		fillerPanel.add(reallyFillerPanel);
		fillerPanel.setBackground(color);
		fillerPanel.setPreferredSize(new Dimension(100, 100));
		fillerPanel.setMaximumSize(fillerPanel.getPreferredSize());
		
		
		
		JPanel leftMenu = new JPanel(new GridLayout(1, 4));
		leftMenu.setPreferredSize(new Dimension(1000, 100));
		leftMenu.setMaximumSize(leftMenu.getPreferredSize());
		leftMenu.setOpaque(false);
		
		
		filler0 = new menuButton("Front", listen, "frontCard");
		pressedButton = filler0;
		filler0.setOpaque(true);
		filler0.setForeground(GUItools.getCoolcolor());
		filler0.setBackground(Color.white);
		
		filler1 = new menuButton("Stillinger", listen, "jobCard");
		filler2 = new menuButton("S�kere", listen, "searchCard");
		filler3 = new menuButton("Ny stilling", listen, "newPosCard");
		filler4 = new menuButton("Meldinger", listen, "inboxCard");
		
		leftMenu.add(filler0);
		leftMenu.add(filler1);
//		leftMenu.add(filler2);
		leftMenu.add(filler3);
		leftMenu.add(filler4);
		
		fillerPanel.add(leftMenu);
		JPanel reallyFillerPanel1 = new JPanel();
		reallyFillerPanel1.setOpaque(false);
		fillerPanel.add(reallyFillerPanel1);
		
		add(fillerPanel, BorderLayout.NORTH);
		
	}

	protected class menuButton extends JButton {
		
		public menuButton(String content, MainListener listen, String cardName) {
			super(content);
			setName(cardName);
			addActionListener(listen);
			setContentAreaFilled(false);
			setOpaque(false);
			setBorderPainted(true);
			setForeground(Color.white);
			setBackground(GUItools.getCoolcolor());
			addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					pressedButton.setForeground(Color.white);
					pressedButton.setBackground(GUItools.getCoolcolor());
					pressedButton = (JButton) e.getSource();
					setOpaque(true);
					setForeground(GUItools.getCoolcolor());
					setBackground(Color.white);
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					if(pressedButton != e.getSource()) {
					setForeground(Color.white);
					setOpaque(false);
					}
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					setOpaque(true);
					setForeground(GUItools.getCoolcolor());
					setBackground(Color.white);
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
//			setBorder(BorderFactory.createMatteBorder(0, 1, 0, 1, color.darkGray));
			setBorder(BorderFactory.createEmptyBorder());
			
		}
		
		
	}
	
	protected void resetMenuColors() {
		
		filler0.setForeground(Color.white);
		filler0.setOpaque(false);
		filler1.setForeground(Color.white);
		filler1.setOpaque(false);
		filler2.setForeground(Color.white);
		filler2.setOpaque(false);
		filler3.setForeground(Color.white);
		filler3.setOpaque(false);
		filler4.setForeground(Color.white);
		filler4.setOpaque(false);
		
	
	}
	
	/**
	 * @return the listen
	 */
	public MainListener getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(MainListener listen) {
		this.listen = listen;
	}

	/**
	 * @return the searchCard
	 */
	public ApplicantCard getSearchCard() {
		return searchCard;
	}

	/**
	 * @param searchCard the searchCard to set
	 */
	public void setSearchCard(ApplicantCard searchCard) {
		this.searchCard = searchCard;
	}



	/**
	 * @return the jobCard
	 */
	public PositionOverviewCard getJobCard() {
		return jobCard;
	}



	/**
	 * @param jobCard the jobCard to set
	 */
	public void setJobCard(PositionOverviewCard jobCard) {
		this.jobCard = jobCard;
	}



	/**
	 * @return the cardHolder
	 */
	public JPanel getCardHolder() {
		return cardHolder;
	}



	/**
	 * @param cardHolder the cardHolder to set
	 */
	public void setCardHolder(JPanel cardHolder) {
		this.cardHolder = cardHolder;
	}


	/**
	 * @return the color
	 */
	public static Color getColor() {
		return color;
	}



	/**
	 * @return the filler1
	 */
	public JButton getFiller1() {
		return filler1;
	}






	/**
	 * @return the filler2
	 */
	public JButton getFiller2() {
		return filler2;
	}






	public NewPositionCard getNewPosCard() {
		return newPosCard;
	}



	public void setNewPosCard(NewPositionCard newPosCard) {
		this.newPosCard = newPosCard;
	}



	public JButton getFiller3() {
		return filler3;
	}






	/**
	 * @return the inboxCard
	 */
	public InboxCard getInboxCard() {
		return inboxCard;
	}



	/**
	 * @param inboxCard the inboxCard to set
	 */
	public void setInboxCard(InboxCard inboxCard) {
		this.inboxCard = inboxCard;
	}



	/**
	 * @return the filler4
	 */
	public JButton getFiller4() {
		return filler4;
	}



	/**
	 * @return the filler0
	 */
	public menuButton getFiller0() {
		return filler0;
	}


	/**
	 * @return the frontCard
	 */
	public FrontCard getFrontCard() {
		return frontCard;
	}


	/**
	 * @return the pressedButton
	 */
	public JButton getPressedButton() {
		return pressedButton;
	}


	/**
	 * @param pressedButton the pressedButton to set
	 */
	public void setPressedButton(JButton pressedButton) {
		this.pressedButton = pressedButton;
	}






	
}
