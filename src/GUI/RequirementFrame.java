package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import data.Categories;
import tools.GUItools;

public class RequirementFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private NewPositionCard newPosPanel;
	private JPanel upperPanel;
	private MainListener listen;
	private JList<Object> categoryList;
	private JList<String> subCategoryList;
	private JTextField selectedStudy;
	private JButton submit;
	private JTable reqTable;
	private JRadioButton erfaring;
	private JRadioButton studier;
	private JComboBox<String> level;
	private JTextField erfaringAar;
	
	public RequirementFrame(NewPositionCard newPosPanel, MainListener listen) {
		
		setTitle("Stillingskrav");
		setLayout(new BorderLayout());
		setSize(700, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ImageIcon icon = new ImageIcon("bullseye.png");
		setIconImage(icon.getImage());
		this.newPosPanel = newPosPanel;
		this.listen = listen;
		
		compWindow();
		
		setVisible(true);
		
		
		
	}
	
	private class CustomListRenderer extends DefaultListCellRenderer {
		
		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			Component c  = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if(isSelected) {
				c.setBackground(GUItools.getCoolcolor());
			}
			return c;
		}
		
	}
	
	private void compWindow() {
		
		
		
		Object[] categoryArray = listen.getHouse().getCategories().toArray();
		
		
		categoryList = new JList<>(categoryArray);
		categoryList.setCellRenderer(new CustomListRenderer());
		categoryList.setPreferredSize(new Dimension(250, 200));
		subCategoryList = new JList<>();
		subCategoryList.setCellRenderer(new CustomListRenderer());
		subCategoryList.setPreferredSize(new Dimension(250, 200));
		
		
		categoryList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!categoryList.getValueIsAdjusting()) {
					if(categoryList.getSelectedIndex() != -1) {
					selectedStudy.setText("");
					Categories selectedCategory = (Categories) categoryList.getSelectedValue();
					DefaultListModel<String> model = new DefaultListModel<>();
					for(String subCategory : selectedCategory.getSubCategories()) {
						model.addElement(subCategory);
					}
					subCategoryList.setModel(model);
					
					
					}
				
				}
				
			}
		});
		
		subCategoryList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!subCategoryList.getValueIsAdjusting()) {
					
					if(subCategoryList.getSelectedIndex() != -1) {
						
						selectedStudy.setText(subCategoryList.getSelectedValue());
						
						
					}
					
				}
				
				
			}
		});
		
		
		
		JSplitPane splitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, categoryList, subCategoryList);
		
		
		add(splitPanel, BorderLayout.CENTER);		
		add(buttonPanel(categoryList, subCategoryList), BorderLayout.SOUTH);
		
		
		
		
	
	}
	
	private class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(e.getSource().equals(erfaring)) {
				erfaringAar.setVisible(true);
				level.setVisible(false);
			}
			else if(e.getSource().equals(studier)) {
				erfaringAar.setVisible(false);
				level.setVisible(true);
			}
			
		}
		
		
	}
	
	private Component buttonPanel(JList<Object> categoryList, JList<String> subCategoryList) {
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(120, 30));
		
		ButtonGroup group = new ButtonGroup();
		erfaring = new JRadioButton("Erfaring");
		erfaring.addActionListener(new Listener());
		studier = new JRadioButton("Studier");
		studier.addActionListener(new Listener());
		
		group.add(erfaring);
		group.add(studier);
		erfaring.setSelected(true);
		
		buttonPanel.add(erfaring);
		buttonPanel.add(studier);
		
		
		selectedStudy = new JTextField();
		selectedStudy.setPreferredSize(new Dimension(200, 20));
		selectedStudy.setEditable(false);
		
		level = new JComboBox<>(new String[] {"�rsstudium", "Bachelor", "Master", "Doktor"});
		level.setVisible(false);
		level.setPreferredSize(new Dimension(120, 20));
		level.setBackground(Color.white);
		
		
		erfaringAar = new JTextField("�rs erfaring");
		erfaringAar.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(erfaringAar.getText().isEmpty()) {
					erfaringAar.setText("�rs erfaring");
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				erfaringAar.setText("");
				
			}
		});
		erfaringAar.setPreferredSize(new Dimension(120, 20));
		
		
		submit = new JButton("Legg til");
		submit.setPreferredSize(new Dimension(100, 20));
		submit.setBackground(GUItools.getCoolcolor());
		submit.addActionListener(new ActionListener() {

			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(selectedStudy.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Studieretning ikke valgt", "Feil", JOptionPane.ERROR_MESSAGE);
				}
				else if(erfaring.isSelected() && !erfaringAar.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Vennligst fyll inn antall �rs erfaring", "Feil", JOptionPane.ERROR_MESSAGE);
				}
				else {
					if(erfaring.isSelected()) {
				newPosPanel.addRequirement(selectedStudy.getText(), categoryList.getSelectedValue().toString(), erfaringAar.getText(), true);
					}
					else {
						newPosPanel.addRequirement(selectedStudy.getText(), categoryList.getSelectedValue().toString(), level.getSelectedItem().toString(), false);
						
					}
				dispose();
				}
				
			}
		});
		
		
		buttonPanel.add(selectedStudy);
		buttonPanel.add(erfaringAar);
		buttonPanel.add(level);
		buttonPanel.add(submit);
		
		
		
		return buttonPanel;
	}
	
	
	

	/**
	 * @return the newPosPanel
	 */
	public JPanel getNewPosPanel() {
		return newPosPanel;
	}

	

	/**
	 * @return the listen
	 */
	public MainListener getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(MainListener listen) {
		this.listen = listen;
	}

	/**
	 * @return the categoryList
	 */
	public JList<Object> getCategoryList() {
		return categoryList;
	}

	/**
	 * @param categoryList the categoryList to set
	 */
	public void setCategoryList(JList<Object> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * @return the subCategoryList
	 */
	public JList<String> getSubCategoryList() {
		return subCategoryList;
	}

	/**
	 * @param subCategoryList the subCategoryList to set
	 */
	public void setSubCategoryList(JList<String> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}

	/**
	 * @return the selectedStudy
	 */
	public JTextField getSelectedStudy() {
		return selectedStudy;
	}

	/**
	 * @param selectedStudy the selectedStudy to set
	 */
	public void setSelectedStudy(JTextField selectedStudy) {
		this.selectedStudy = selectedStudy;
	}

	/**
	 * @return the submit
	 */
	public JButton getSubmit() {
		return submit;
	}

	/**
	 * @param submit the submit to set
	 */
	public void setSubmit(JButton submit) {
		this.submit = submit;
	}

	/**
	 * @return the upperPanel
	 */
	public JPanel getUpperPanel() {
		return upperPanel;
	}

	/**
	 * @param upperPanel the upperPanel to set
	 */
	public void setUpperPanel(JPanel upperPanel) {
		this.upperPanel = upperPanel;
	}


	/**
	 * @return the reqTable
	 */
	public JTable getReqTable() {
		return reqTable;
	}

	/**
	 * @param reqTable the reqTable to set
	 */
	public void setReqTable(JTable reqTable) {
		this.reqTable = reqTable;
	}
	
	
	
}
