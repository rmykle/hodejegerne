package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import data.Stilling;
import guipanels.PositionPanel;
import tools.GUItools;

public class PositionOverviewCard extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private MainListener listen;
	private JButton editButton, nyStilling;
	private JPanel jobPanel, emptyPanel;
	private JPanel test;

	public PositionOverviewCard(MainListener listen) {
		
		this.listen = listen;
		setLayout(new BorderLayout());
		setupEmptyPanel();
		setupContent();
		/**
		 *JScrollPane tableScroll = new JScrollPane(createTable());
		add(tableScroll, BorderLayout.CENTER); 
		 */
		setupJobCard();
		
		
//		addButtomMenu();
//		addRightMenu();
	
		
	}
	
	private void setupContent() {
		
		jobPanel = new JPanel(new GridBagLayout());
		
		JPanel posHolder = new JPanel(new BorderLayout());
		posHolder.add(jobPanel, BorderLayout.NORTH);
		
		JScrollPane scroll = new JScrollPane(posHolder, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		test = new JPanel(new GridLayout(1, 1));
		test.add(scroll);
		scroll.setBorder(BorderFactory.createEmptyBorder());
		scroll.setMaximumSize(scroll.getParent().getSize());
		
	}

	private void setupEmptyPanel() {
		
		emptyPanel = new JPanel();
		emptyPanel.setLayout(new BoxLayout(emptyPanel, BoxLayout.PAGE_AXIS));
		JLabel label = new JLabel("Ingen stillinger");
		label.setForeground(GUItools.getCoolcolor());
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		emptyPanel.add(label);
		
		emptyPanel.add(Box.createRigidArea(new Dimension(10, 10)));
		
		nyStilling = new JButton("Opprett ny stilling");
		nyStilling.setAlignmentX(Component.CENTER_ALIGNMENT);
		nyStilling.addActionListener(listen);
		nyStilling.setName("jobCard");
		nyStilling.setBackground(GUItools.getCoolcolor());
		emptyPanel.add(nyStilling);
		
		label.setFont(new Font("Verdana", Font.ITALIC, 30));
		
	}


	private void setupJobCard() {
		removeAll();
		if(listen.getTools().getLoggedInCompany().getUtlysteStillinger().size() > 0) {
			refreshPanels();
			add(test, BorderLayout.CENTER);
		}
		else {
			add(emptyPanel, BorderLayout.CENTER);
			
		}
		
		validate();
		repaint();
		
		
		
		}
	
	
	protected void refreshPanels() {
		jobPanel.removeAll();
		
		GridBagConstraints cons = GUItools.getDefaultConstraints();
		ArrayList<Stilling> stillinger = new ArrayList<>(listen.getTools().getLoggedInCompany().getUtlysteStillinger().values());
		Collections.sort(stillinger);
		
		if(stillinger.size() == 0) {
			add(emptyPanel, BorderLayout.CENTER);
		}
		else {
			for(Stilling stilling : stillinger) {
				PositionPanel posPanel = new PositionPanel(stilling, listen);
				
				jobPanel.add(posPanel, cons, 0);
				
				
			}
		}
		validate();
		repaint();
		
		
	}
	
	/**
	 * @return the listen
	 */
	public MainListener getListen() {
		return listen;
	}

	/**
	 * @param listen the listen to set
	 */
	public void setListen(MainListener listen) {
		this.listen = listen;
	}


	/**
	 * @return the editButton
	 */
	public JButton getEditButton() {
		return editButton;
	}

	/**
	 * @param editButton the editButton to set
	 */
	public void setEditButton(JButton editButton) {
		this.editButton = editButton;
	}

	/**
	 * @return the nyStilling
	 */
	public JButton getNyStilling() {
		return nyStilling;
	}

	
	
	
}
