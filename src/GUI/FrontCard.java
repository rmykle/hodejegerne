package GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import data.Employee;
import data.Employeesorter;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import tools.GUItools;
import tools.Main;

public class FrontCard extends JPanel {

	private MainListener listen;
	private JPanel watcherPanel;
	
	public FrontCard(MainListener listen) {
		this.listen = listen;
		setLayout(new BorderLayout());
		
		
		createHeader();
		createMiddle();
		
		
		
	}

	private void createMiddle() {
		
		JPanel middle = new JPanel(new GridLayout(1, 2));
		JPanel outerLeft = new JPanel(new BorderLayout());
		outerLeft.add(leftSide());
		outerLeft.add(employeeToWatch(), BorderLayout.SOUTH);
		middle.add(outerLeft, BorderLayout.CENTER);
		middle.add(rightSide());
		
		add(middle, BorderLayout.CENTER);
		
	}
	
	
	private Component rightSide() {
		JPanel rightSide = new JPanel();
		rightSide.setBackground(java.awt.Color.white);
		JFXPanel fxPanel = new JFXPanel();
		fxPanel.setBackground(java.awt.Color.RED);
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				
				Scene scene = createScene();
				fxPanel.setScene(scene);
				
			}

			
		});
		rightSide.add(fxPanel);
		
		
		
		
		return rightSide;
		
	}
	private Scene createScene() {
		
		Scene scene = new Scene(new Group());
		
		HashMap<Integer, Integer> result = Employeesorter.getcompetencyMap(listen.getHouse().getEmployees().values(), listen.getTools().getLoggedInCompany());
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        
        
        
        for(Entry<Integer, Integer> results : result.entrySet()) {
        	String kvalikString = String.valueOf(results.getKey());
        	if(results.getKey() ==4) {
        		kvalikString += "+ stillinger";
        	}
        	else  kvalikString += " stillinger";
        	pieChartData.add(new Data(kvalikString, results.getValue()));
        }
        

        final PieChart chart = new PieChart(pieChartData);
        chart.setLabelsVisible(false);
        chart.setTitle("Kvalifikasjonsstatus");
        final Label caption = new Label("");
       
        caption.setTextFill(Color.DARKORANGE);
        caption.setStyle("-fx-font: 24 arial;");
                
        ((Group) scene.getRoot()).getChildren().addAll(chart, caption);
        return scene;
		
	}

	private Component leftSide() {
		
		JPanel leftSide = new JPanel();
		leftSide.setBackground(java.awt.Color.white);
		leftSide.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, GUItools.getCoolcolor()));
		leftSide.setLayout(new BoxLayout(leftSide, BoxLayout.PAGE_AXIS));
		
		String infoString = "";
		infoString += "<br>" + "Antall stillinger registert: "+  listen.getTools().getLoggedInCompany().getUtlysteStillinger().size();
		infoString += "<br>" + "Antall arbeidssøkere: " + listen.getHouse().getEmployees().size(); 
		infoString += "<br>" + "Antall ubesvarte meldinger: " + listen.getMsgBase().ubesvarteMeldinger();
		
		leftSide.add(GUItools.infoGenerator("<br><br><br>"));
		JLabel infoLabel = GUItools.infoGenerator(infoString);
		leftSide.add(GUItools.headerGenerator(listen.getTools().getLoggedInCompany().getNavn()));
		leftSide.add(infoLabel);
		leftSide.add(Box.createRigidArea(new Dimension(10, 30)));
		
		
		
		return leftSide;
	}
	
	protected void updateEmployeeToWatch(Employee employee) {
		
		
		JButton contact = new JButton("Kontakt");
		contact.setBackground(GUItools.getCoolcolor());
		contact.setName("inboxCard");
		contact.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e) {
				listen.getMainFrame().setPressedButton(listen.getMainFrame().getFiller4());
				listen.getMainFrame().resetMenuColors();
				
				listen.getMainFrame().getFiller4().setOpaque(true);
				listen.getMainFrame().getFiller4().setBackground(java.awt.Color.WHITE);
				listen.getMainFrame().getFiller4().setForeground(GUItools.getCoolcolor());
				
				listen.getMainFrame().getInboxCard().setIdSelected(employee.getId());
				listen.changeCard(e);
				listen.getMainFrame().setPressedButton(listen.getMainFrame().getFiller4());
				
			}
		});
		
		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		JLabel infoLabel = GUItools.infoGenerator(employee.competancyString() +"<br>");
		left.add(infoLabel);
		
		left.add(contact);
		
		watcherPanel.add(left, BorderLayout.CENTER);
		
			
			
			URL url = Main.class.getResource("/shield.png");
		
			JLabel picLabel = new JLabel(new ImageIcon(url));
			JPanel picturePanel = new JPanel();
			picturePanel.setPreferredSize(new Dimension(200, 200));
			picturePanel.add(picLabel);
			watcherPanel.add(picturePanel, BorderLayout.EAST);
			
		
		
		
		
		
	}
	private JPanel employeeToWatch() {
		watcherPanel = new JPanel();
		watcherPanel.setLayout(new BorderLayout());
		watcherPanel.setPreferredSize(new Dimension(300, 200));
		watcherPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, GUItools.getCoolcolor()));
		watcherPanel.setBackground(GUItools.getLightgrey());
		
		//Content
		JLabel header = GUItools.headerGenerator("I kikkertsikte");
		JPanel headerWrapper = new JPanel(new BorderLayout());
		headerWrapper.add(header, BorderLayout.WEST);
		watcherPanel.add(headerWrapper, BorderLayout.NORTH);
		
		updateEmployeeToWatch(Employeesorter.getGreatestCandidate(listen.getHouse()));
		return watcherPanel;
		
	}

	private void createHeader() {
		
		JLabel header = GUItools.headerGenerator("Hodejeger");
		header.setOpaque(true);
		header.setBackground(java.awt.Color.white);
		header.setFont(new Font("Verdana", Font.PLAIN, 22));
		add(header, BorderLayout.NORTH);
	}
	
	
	
	
}
