package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.sun.javafx.css.CssError.StringParsingError;

import data.Experience;
import data.Study;
import data.Study.Grade;
import databasehandling.ErfaringDAO;
import tools.GUItools;

public class NewPositionCard extends JPanel
{
	
	private static final long serialVersionUID = 1L;
	private MainListener listen;
	private JPanel inputPanel;
	private JButton reqButton, submit, cancel;
	private JTextField tittel, sokeFrist;
	private JTextArea beskrivelse;
	private JPanel studyReqPanel;
	private JPanel outerReqPanel;
	private JPanel expReqPanel;
	private JDatePickerImpl datePicker;
	private HashMap<Integer, Study> studyReq;
	private HashMap<Integer, Experience> erfaring;
	private int lastID = 0;
	
	public NewPositionCard(MainListener listen) {
	
		this.listen = listen;
		this.inputPanel = new JPanel();
		studyReq = new HashMap<>();
		erfaring = new HashMap<>();
//		studyReq = new ArrayList<>();
//		erfaring = new ArrayList<>();
		setLayout(new BorderLayout());
		setBackground(Color.white);
		this.inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.PAGE_AXIS));
		inputPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(inputPanel, BorderLayout.WEST);
		
		
		
		setupCard();
		descriptionArea();
		competancyPanel();
	}
	private void competancyPanel() {
		
		JPanel compPanel = new JPanel(new BorderLayout());	
		compPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 0, GUItools.getCoolcolor()));
		compPanel.setPreferredSize(new Dimension(400, 100));
		
		
		compPanel.add(reqPanel(), BorderLayout.NORTH);
		
		
		JPanel outerButton = new JPanel(new FlowLayout());
		outerButton.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, GUItools.getCoolcolor()));
		reqButton = new JButton("Legg til krav");
		reqButton.setBackground(GUItools.getCoolcolor());
		outerButton.add(reqButton);
		compPanel.add(outerButton, BorderLayout.SOUTH);
		reqButton.addActionListener(listen);
		
		add(compPanel, BorderLayout.EAST);
		
	}
	
	
	protected void addRequirement(String navn, String category, String level, boolean exp) {
		
		
		JPanel newPanel = new JPanel(new BorderLayout());
		newPanel.add(GUItools.infoGenerator(category + " - " + navn + " - " + level), BorderLayout.WEST);
		JButton fjern = new JButton("Fjern");
		fjern.setName(String.valueOf(lastID));
		fjern.setBackground(GUItools.getCoolcolor());
		fjern.setPreferredSize(new Dimension(70, 20));
		newPanel.add(fjern, BorderLayout.EAST);
		
		fjern.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				newPanel.getParent().remove(newPanel);
				erfaring.remove(Integer.valueOf(fjern.getName()));
				studyReq.remove(Integer.valueOf(fjern.getName()));
				studyReqPanel.revalidate();
				expReqPanel.revalidate();
			}

			
		});
		
		
		
		
		GridBagConstraints cons = new GridBagConstraints();
		cons.gridwidth = GridBagConstraints.REMAINDER;
        cons.weightx = 1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.anchor = GridBagConstraints.NORTH;
        cons.insets = new Insets(0, 0, 2, 0);
        
        JPanel addingPanel = null;
        if(exp) {
        	addingPanel = expReqPanel;
        	erfaring.put(lastID++, new Experience(category, navn, Integer.valueOf(level)));
        	if(0 == addingPanel.getComponentCount()) {
        			
        	       expReqPanel.add(GUItools.headerGenerator("Erfaring"), cons, 0);
        	}
        }
        else {
        	addingPanel = studyReqPanel;
        	studyReq.put(lastID++, new Study(level, category, navn));
        	if(0 == addingPanel.getComponentCount()) {
        		
        		studyReqPanel.add(GUItools.headerGenerator("Studier"), cons, 0);
        	}
        }
        
        
		addingPanel.add(newPanel, cons);
		addingPanel.revalidate();
		
	}

	private Component reqPanel() {
		
		outerReqPanel = new JPanel(new GridBagLayout());
		
		studyReqPanel = new JPanel(new GridBagLayout());
		expReqPanel = new JPanel(new GridBagLayout());
		
		
//		reqPanel.setLayout(new BoxLayout(reqPanel, BoxLayout.PAGE_AXIS));
		
		GridBagConstraints cons = new GridBagConstraints();
		cons.gridwidth = GridBagConstraints.REMAINDER;
        cons.weightx = 1;
        cons.fill = GridBagConstraints.HORIZONTAL;
        cons.anchor = GridBagConstraints.NORTH;
        cons.insets = new Insets(0, 0, 2, 0);
		
      
        
        outerReqPanel.add(GUItools.headerGenerator("Krav"), cons, 0);
		outerReqPanel.add(studyReqPanel, cons);
		outerReqPanel.add(expReqPanel, cons);
		
		return outerReqPanel;
	}
	private void descriptionArea() {
		
		JPanel descPanel = new JPanel(new BorderLayout());
		descPanel.setOpaque(false);
		
		descPanel.setPreferredSize(new Dimension(100, 300));
		descPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JPanel headerFiller = new JPanel(new BorderLayout());
		headerFiller.setOpaque(false);
		headerFiller.add(GUItools.headerGenerator("Stillingsbeskrivelse:"), BorderLayout.WEST);
		
		descPanel.add(headerFiller, BorderLayout.NORTH);
//		descPanel.add(headerPanel("Stillingsbeskrivelse:"));
		
		beskrivelse = new JTextArea();
		beskrivelse.setLineWrap(true);
		beskrivelse.setWrapStyleWord(true);
		JScrollPane scroll = new JScrollPane(beskrivelse);
		descPanel.add(scroll, BorderLayout.CENTER);
		
		add(descPanel, BorderLayout.SOUTH);
		
	}
	
	
	
	

	private class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "dd/MM/yyyy";
	    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	    @Override
	    public Object stringToValue(String text) throws ParseException {
	        return dateFormatter.parseObject(text);
	    }

	    @Override
	    public String valueToString(Object value) throws ParseException {
	        if (value != null) {
	            Calendar cal = (Calendar) value;
	            return dateFormatter.format(cal.getTime());
	        }

	        return "";
	    }

	}


	private void setupCard() {
		
		JPanel tittelPanel = headerPanel("Tittel");
		tittelPanel.setPreferredSize(new Dimension(250, 20));
		tittelPanel.setMaximumSize(tittelPanel.getPreferredSize());
		inputPanel.add(tittelPanel);
		
		tittel = new JTextField();
		tittel.setPreferredSize(new Dimension(250, 20));
		tittel.setMaximumSize(tittel.getPreferredSize());
		inputPanel.add(tittel);
		
		JPanel fristPanel = headerPanel("S�kefrist");
		fristPanel.setPreferredSize(new Dimension(250, 20));
		fristPanel.setMaximumSize(fristPanel.getPreferredSize());
		inputPanel.add(fristPanel);
		
		//Funker
//		sokeFrist = new JTextField();
//		sokeFrist.setPreferredSize(new Dimension(250, 20));
//		sokeFrist.setMaximumSize(sokeFrist.getPreferredSize());
		
		//Testing
		UtilDateModel model = new UtilDateModel();
		//model.setDate(20,04,2014);
		// Need this...
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setPreferredSize(new Dimension(250, 20));
		datePicker.setBackground(Color.white);
		datePanel.setBackground(Color.white);
		inputPanel.add(datePicker);
		
		
		
		
//		inputPanel.add(sokeFrist);
		
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.setPreferredSize(new Dimension(250, 20));
		buttonPanel.setOpaque(false);
		submit = new JButton("Registrer stilling");
		submit.setName("jobCard");
		submit.setBackground(GUItools.getCoolcolor());
		submit.addActionListener(listen);
		cancel = new JButton("Avbryt");
		cancel.setBackground(GUItools.getCoolcolor());
		cancel.addActionListener(listen);
		
		buttonPanel.add(submit);
		buttonPanel.add(cancel);
		
		inputPanel.add(buttonPanel);
		inputPanel.add(Box.createVerticalGlue());
		
		inputPanel.setOpaque(false);
		
	}
	
	protected boolean validInput() {
		
		if(tittel.getText().isEmpty()) return false;
		if(datePicker.getJFormattedTextField().getText().isEmpty()) return false;
		if(beskrivelse.getText().isEmpty()) return false;
		
		return true;
		
	}
	
	protected void clearFields() {
		
		tittel.setText("");
		beskrivelse.setText("");
		studyReq.clear();
		erfaring.clear();
		
	}
	
	private JPanel headerPanel(String header) {
	
		JPanel headerPanel = new JPanel(new BorderLayout()) ;
		headerPanel.setOpaque(false);
		
		headerPanel.add(GUItools.headerGenerator(header), BorderLayout.WEST);
		
		
		return headerPanel;
	
	}
	/**
	 * @return the reqButton
	 */
	public JButton getReqButton() {
		return reqButton;
	}
	/**
	 * @return the submit
	 */
	public JButton getSubmit() {
		return submit;
	}
	/**
	 * @return the cancel
	 */
	public JButton getCancel() {
		return cancel;
	}
	/**
	 * @return the tittel
	 */
	public JTextField getTittel() {
		return tittel;
	}
	/**
	 * @return the sokeFrist
	 */
	public JTextField getSokeFrist() {
		return sokeFrist;
	}
	/**
	 * @return the beskrivelse
	 */
	public JTextArea getBeskrivelse() {
		return beskrivelse;
	}
	/**
	 * @return the datePicker
	 */
	public JDatePickerImpl getDatePicker() {
		return datePicker;
	}
	/**
	 * @return the studyReq
	 */
	public HashMap<Integer, Study> getStudyReq() {
		return studyReq;
	}
	/**
	 * @param studyReq the studyReq to set
	 */
	public void setStudyReq(HashMap<Integer, Study> studyReq) {
		this.studyReq = studyReq;
	}
	/**
	 * @return the erfaring
	 */
	public HashMap<Integer, Experience> getErfaring() {
		return erfaring;
	}
	/**
	 * @param erfaring the erfaring to set
	 */
	public void setErfaring(HashMap<Integer, Experience> erfaring) {
		this.erfaring = erfaring;
	}
	
	
	

}
