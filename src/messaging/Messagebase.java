package messaging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import data.Company;
import data.Employee;
import tools.Datetools;

public class Messagebase {
	
	private Company company;
	private HashMap<Employee, ArrayList<Message>> messages;
	
	public Messagebase(Company company) {
		this.company = company;
		this.messages = new HashMap<>();
	}
	
	public void newMessage(Message message) {
		
		if(messages.containsKey(message.getEmployee())) {
				messages.get(message.getEmployee()).add(message);
		}
		else {
			ArrayList<Message> newMessages = new ArrayList<>();
			newMessages.add(message);
			messages.put(message.getEmployee(), newMessages);
			
			
		}
		
		
	}
	
	public ArrayList<Employee> getSortedRecievers() {
		
		ArrayList<Employee> sortedList = new ArrayList<>(messages.keySet());
		
		Comparator<Employee> comperator = new Comparator<Employee>() {
			
			@Override
			public int compare(Employee o1, Employee o2) {
				if(getLatestMessage(o1).getDate().before(getLatestMessage(o2).getDate())) return -1;
				else if(getLatestMessage(o1).getDate().after(getLatestMessage(o2).getDate())) return 1;
				return 0;
			}
		};
		
		Collections.sort(sortedList, comperator);
		
		return sortedList;
		
	}
	
	public int ubesvarteMeldinger() {
		int counter = 0;
		for(ArrayList<Message> entry : messages.values()) {
			
			Collections.sort(entry);
			
			if(!entry.get(entry.size()-1).isCompanySends()) {
				
				counter++;
				
			}
			
			
			
			
			
		}
		
		return counter;
		
	}
	
	public Message getLatestMessage(Employee employee) {
		
		Collections.sort(messages.get(employee));
		
		if(messages.get(employee).size() > 0 ) { 
		return Collections.max(messages.get(employee));
		}
		return new Message(company, employee, new Date(), true, "");
		
	}
	
	public ArrayList<Message> getSortedMessages(Employee employee) {
		return messages.get(employee);
		
		
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the messages
	 */
	public HashMap<Employee, ArrayList<Message>> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(HashMap<Employee, ArrayList<Message>> messages) {
		this.messages = messages;
	}
	
	

}
