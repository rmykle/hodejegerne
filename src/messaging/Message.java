package messaging;

import java.util.Date;

import data.Company;
import data.Employee;
import guipanels.EmployeePanel;
import tools.Datetools;

public class Message implements Comparable<Message> {
	
	private Company company;
	private Employee employee;
	private boolean companySends;
	private Date date;
	private String content;
	
	
	/**
	 * @param company
	 * @param employee
	 * @param companySends
	 */
	public Message(Company company, Employee employee, Date date, boolean companySends, String content) {
		super();
		this.company = company;
		this.employee = employee;
		this.companySends = companySends;
		this.content = content;
		this.date = date;
	}
	
	
	@Override
	public int compareTo(Message o) {
		if(date.before(o.getDate())) return -1;
		else if(date.after(o.getDate())) return 1;
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		Message newMessage = (Message) obj;
		if(company.getId() != newMessage.getCompany().getId()) return false;
		if(employee.getId() != newMessage.getEmployee().getId()) return false;
		if(companySends != newMessage.isCompanySends())
		if(!content.equals(newMessage.getContent())) return false;
		
		return true;
	}
	
	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}
	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}
	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	/**
	 * @return the companySends
	 */
	public boolean isCompanySends() {
		return companySends;
	}
	/**
	 * @param companySends the companySends to set
	 */
	public void setCompanySends(boolean companySends) {
		this.companySends = companySends;
	}








	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}








	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}








	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}








	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Message [company=" + company.getNavn() + ", employee=" + employee.getId() + ", companySends=" + companySends + ", date="
				+ date + ", content=" + content + "]";
	}


	
	
	

}
